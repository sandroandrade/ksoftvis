/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "radialgraphicstreeview.h"

#include <QtCore/QDebug>
#include <QtCore/qmath.h>
#include <QtCore/QVariant>

#include <QtGui/QStandardItem>
#include <QtGui/QStandardItemModel>

#include <qglobal.h>

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QGraphicsEllipseItem>
#else
#include <QtWidgets/QGraphicsEllipseItem>
#endif

#include "graphicsellipseringitem.h"

#define PI 3.14159265358979

Q_DECLARE_METATYPE(QModelIndex)

RadialGraphicsTreeView::RadialGraphicsTreeView(QWidget *parent) :
    IGraphicsCompoundTreeView(parent),
    m_viewFlags(ShowNormalRadialViewNode |
                ShowNormalRadialViewEdge |
                ShowMirroredRadialView |
                ShowAdjacencyModel),
    m_nodeRadius(1.0),
    m_nodePen(Qt::black),
    m_nodeBrush(Qt::yellow),
    m_edgePen(Qt::black),
    m_interLevelSpacing(15),
    m_interSpanSpacing((10*PI)/180),
    m_mirroredRadiusIncrementFactor(0.25),
    m_bundlingStrengthFactor(0.85),
    m_adjacencyEdgePenWidth(0.5)
{
}

RadialGraphicsTreeView::~RadialGraphicsTreeView()
{
}

void RadialGraphicsTreeView::setViewFlag(RadialGraphicsTreeView::ViewFlag viewFlag, bool enabled)
{
    if (enabled)
        m_viewFlags |= viewFlag;
    else
        m_viewFlags &= ~viewFlag;
}

void RadialGraphicsTreeView::setViewFlags(ViewFlags viewFlags)
{
    m_viewFlags = viewFlags;
}

void RadialGraphicsTreeView::setNodeRadius(qreal nodeRadius)
{
    m_nodeRadius = nodeRadius;
}

void RadialGraphicsTreeView::setNodePen(const QPen &nodePen)
{
    m_nodePen = nodePen;
}

void RadialGraphicsTreeView::setNodeBrush(const QBrush &nodeBrush)
{
    m_nodeBrush = nodeBrush;
}

void RadialGraphicsTreeView::setEdgePen(const QPen &edgePen)
{
    m_edgePen = edgePen;
}

void RadialGraphicsTreeView::setInterLevelSpacing(qreal interLevelSpacing)
{
    m_interLevelSpacing = interLevelSpacing;
}

void RadialGraphicsTreeView::setInterSpanSpacing(qreal interSpanSpacing)
{
    m_interSpanSpacing = interSpanSpacing;
}

void RadialGraphicsTreeView::setMirroredRadiusIncrementFactor(qreal mirroredRadiusIncrementFactor)
{
    m_mirroredRadiusIncrementFactor = mirroredRadiusIncrementFactor;
}

void RadialGraphicsTreeView::setBundlingStrengthFactor(qreal bundlingStrengthFactor)
{
    m_bundlingStrengthFactor = qMin(qMax(bundlingStrengthFactor, 0.0), 1.0);
}

void RadialGraphicsTreeView::setAdjacencyEdgePenWidth(qreal adjacencyEdgePenWidth)
{
    m_adjacencyEdgePenWidth = adjacencyEdgePenWidth;
}

void RadialGraphicsTreeView::updateTreeScene()
{
    if (!m_treeModel || !m_treeModelRootIndex.isValid())
        return;

    m_maxRadius = 0.0;
    m_spanOf.clear();
    m_radiusOf.clear();
    m_childrenOf.clear();
    m_mirroringOf.clear();

    m_itemForIndex.clear();

    reset();
    m_pathItems.clear();

    updateTreeSceneStep(this->m_treeModelRootIndex, 0.0, 0.0, 2*PI, 0);
    if (m_viewFlags & ShowMirroredRadialView)
    {
        qreal maxLevel = m_maxRadius/m_interLevelSpacing;
        m_mirroredInterLevelSpacing = (m_maxRadius*m_mirroredRadiusIncrementFactor)/(maxLevel-1);
        qreal invertedMaxRadius = m_maxRadius*(1+m_mirroredRadiusIncrementFactor)+m_mirroredInterLevelSpacing;
        buildMirroredTreeSceneStep(m_itemForIndex[m_treeModelRootIndex], invertedMaxRadius);
        if (m_viewFlags & ShowAdjacencyModel)
            updateAdjacencyScene();
    }
}

void RadialGraphicsTreeView::updateAdjacencyScene()
{
    if (!m_adjacencyModel)
        return;

    foreach(QGraphicsPathItem *pathItem, m_pathItems)
        m_scene->removeItem(pathItem);

    m_pathItems.clear();

    int rowCount = m_adjacencyModel->rowCount(QModelIndex());
    for (int row = 0; row < rowCount; ++row)
    {
        QModelIndex source = qvariant_cast<QModelIndex>(m_adjacencyModel->index(row, 0, QModelIndex()).data(Qt::UserRole));
        QModelIndex target = qvariant_cast<QModelIndex>(m_adjacencyModel->index(row, 1, QModelIndex()).data(Qt::UserRole));
        QPainterPath lcaPath = lcaPathBetween(source, target);
        QLinearGradient gradient(lcaPath.pointAtPercent(0), lcaPath.pointAtPercent(1));
        gradient.setColorAt(0, QColor(73, 215, 48, 192));
        gradient.setColorAt(1, QColor(231, 21, 6, 192));
        QGraphicsPathItem *pathItem = m_scene->addPath(lcaPath);
        pathItem->setPen(QPen(gradient, m_adjacencyEdgePenWidth));
        pathItem->setZValue(-1);
        m_pathItems.append(pathItem);
    }
}

void RadialGraphicsTreeView::updateTreeSceneStep(const QModelIndex &index, qreal radius, qreal startAngle, qreal spanAngle, QGraphicsItem *parentItem, int maxLevel)
{
    if (radius > m_maxRadius)
        m_maxRadius = radius;

    if (radius == m_interLevelSpacing)
    {
        startAngle += m_interSpanSpacing/2.0;
        spanAngle = qMax(spanAngle-m_interSpanSpacing, 0.0);
    }

    if (m_viewFlags & ShowNormalRadialViewLevelCircles)
        m_scene->addEllipse(-radius, -radius, 2.0*radius, 2.0*radius, QPen(Qt::black));

    QGraphicsEllipseItem *item = 0;
    if (m_viewFlags & ShowNormalRadialViewNode)
        item = m_scene->addEllipse(-m_nodeRadius, -m_nodeRadius, 2*m_nodeRadius, 2*m_nodeRadius, m_nodePen, m_nodeBrush);
    else
        item = m_scene->addEllipse(0, 0, 0, 0);
    QPointF itemCenter(radius*qCos(startAngle+spanAngle/2.0), -radius*qSin(startAngle+spanAngle/2.0));
    item->setPos(itemCenter);
    item->setToolTip(index.data(Qt::DisplayRole).toString());
    item->setZValue(2);

    m_itemForIndex[index] = item;

    if (parentItem)
    {
        if (m_viewFlags & ShowNormalRadialViewEdge)
            m_scene->addLine(QLineF(parentItem->pos(), itemCenter), m_edgePen)->setZValue(1);
        if (m_viewFlags & ShowMirroredRadialView)
        {
            m_childrenOf.insertMulti(parentItem, item);
            m_radiusOf.insert(item, radius);
        }
    }
    parentItem = item;

    if (m_viewFlags & ShowMirroredRadialView)
        m_spanOf.insert(item, ItemSpan(startAngle, spanAngle));

    qreal totalChildren = 0.0;
    int rowCount = m_treeModel->rowCount(index);

    if (rowCount == 0 || (maxLevel != -1 && (radius/m_interLevelSpacing) == maxLevel))
        return;

    for (int row = 0; row < rowCount; ++row)
        totalChildren += subTreeSize(m_treeModel->index(row, 0, index));
    for (int row = 0; row < rowCount; ++row)
    {
        QModelIndex child = m_treeModel->index(row, 0, index);
        qreal spanDelta;
        spanDelta = ((subTreeSize(child)/totalChildren)*(spanAngle));
        updateTreeSceneStep(child, radius+m_interLevelSpacing, startAngle, spanDelta, parentItem, maxLevel);
        startAngle += spanDelta;
    }
}

int RadialGraphicsTreeView::subTreeSize(const QModelIndex &index) const
{
    int descendants = 1;
    int rowCount = m_treeModel->rowCount(index);
    for (int row = 0; row < rowCount; ++row)
        descendants += subTreeSize(m_treeModel->index(row, 0, index));
    return descendants;
}

void RadialGraphicsTreeView::buildMirroredTreeSceneStep(QGraphicsItem *originalItem, qreal mirroredRadius)
{
    if (m_childrenOf.values(originalItem).size() == 0)
    {
        int itemLevel = m_radiusOf.value(originalItem)/m_interLevelSpacing;
        qreal scale = (m_maxRadius+m_mirroredInterLevelSpacing+(m_maxRadius/m_interLevelSpacing-itemLevel-1)*m_mirroredInterLevelSpacing)/m_radiusOf.value(originalItem);
        m_mirroringOf.insert(originalItem, originalItem->pos()*scale);
    }

    foreach(QGraphicsItem *child, m_childrenOf.values(originalItem))
    {
        qreal ringFactor = 1-((mirroredRadius-m_mirroredInterLevelSpacing)/mirroredRadius);
        ItemSpan span = m_spanOf[child];
        GraphicsEllipseRingItem *item = new GraphicsEllipseRingItem(-mirroredRadius, -mirroredRadius,
                                                                    2*mirroredRadius, 2*mirroredRadius,
                                                                    ((span.first*180)/PI)*16,
                                                                    ((span.second*180)/PI)*16,
                                                                    ringFactor-(ringFactor*0.2));
        item->setBrush(Qt::white);
        item->setToolTip(child->toolTip());
        qreal factor = m_radiusOf.value(originalItem)/m_maxRadius;
        item->setPen(QPen(QBrush(Qt::black), 0.05*(factor)+0.8*(1-factor)));
        m_scene->addItem(item);

        buildMirroredTreeSceneStep(child, mirroredRadius-m_mirroredInterLevelSpacing);
    }
}

QPainterPath RadialGraphicsTreeView::lcaPathBetween(QModelIndex source, QModelIndex target) const
{
    QList<QModelIndex> fromLcaToSource = pathFromRootTo(source);
    QList<QModelIndex> fromLcaToTarget = pathFromRootTo(target);
    QMutableListIterator<QModelIndex> si(fromLcaToSource);
    QMutableListIterator<QModelIndex> ti(fromLcaToTarget);

    while (si.hasNext() && ti.hasNext())
    {
        QModelIndex sindex = si.next();
        QModelIndex tindex = ti.next();
        if ((sindex == tindex) && (si.hasNext() && ti.hasNext()) && (si.peekNext() == ti.peekNext()))
        {
            si.remove();
            ti.remove();
        }
        else
        {
            break;
        }
    }


    QPainterPath path;
    QPointF lcaProjectedPoint = interpolate(m_itemForIndex[source]->pos(), m_itemForIndex[target]->pos(), 0.5);
    addPathFromLcaTo(fromLcaToSource, lcaProjectedPoint, &path);
    addPathFromLcaTo(fromLcaToTarget, lcaProjectedPoint, &path);

    int i = 1;
    qreal size = fromLcaToSource.size();
    QModelIndex p1 = fromLcaToSource.at(i);
    QPointF projectedP1 = interpolate(lcaProjectedPoint, m_itemForIndex[fromLcaToSource.last()]->pos(), i*(1/(size-1)) );
    QPointF projectedP2 = interpolate(m_itemForIndex[p1]->pos(), projectedP1, m_bundlingStrengthFactor);
    path.moveTo(projectedP2);
    i = 0;
    p1 = fromLcaToSource.at(i);
    projectedP1 = interpolate(lcaProjectedPoint, m_itemForIndex[fromLcaToSource.last()]->pos(), i*(1/(size-1)) );
    projectedP2 = interpolate(m_itemForIndex[p1]->pos(), projectedP1, m_bundlingStrengthFactor);
    i = 1;
    p1 = fromLcaToTarget.at(i);
    projectedP1 = interpolate(lcaProjectedPoint, m_itemForIndex[fromLcaToTarget.last()]->pos(), i*(1/(size-1)) );
    QPointF projectedP3 = interpolate(m_itemForIndex[p1]->pos(), projectedP1, m_bundlingStrengthFactor);
    path.cubicTo(projectedP2, projectedP2, projectedP3);

    return path;
}

QList<QModelIndex> RadialGraphicsTreeView::pathFromRootTo(QModelIndex index) const
{
    QList<QModelIndex> path;
    path.append(index);
    while(index.parent() != QModelIndex())
    {
        index = index.parent();
        path.append(index);
    }
    int size = path.size();
    for(int i = 0; i < size/2; ++i)
        path.swap(i, size-i-1);
    return path;
}

void RadialGraphicsTreeView::addPathFromLcaTo(const QList<QModelIndex> &fromLca, const QPointF &lcaProjectedPoint, QPainterPath *path) const
{
    int i = 1;
    qreal size = fromLca.size();
    QModelIndex p1 = fromLca.at(i);
    QPointF projectedP1 = interpolate(lcaProjectedPoint, m_itemForIndex[fromLca.last()]->pos(), i*(1/(size-1)) );
    QPointF projectedP2 = interpolate(m_itemForIndex[p1]->pos(), projectedP1, m_bundlingStrengthFactor);
    path->moveTo(projectedP2);
    i++;
    while (i < size-1) {
        p1 = fromLca.at(i);
        projectedP1 = interpolate(lcaProjectedPoint, m_itemForIndex[fromLca.last()]->pos(), i*(1/(size-1)) );
        projectedP2 = interpolate(m_itemForIndex[p1]->pos(), projectedP1, m_bundlingStrengthFactor);
        p1 = fromLca.at(i+1);
        projectedP1 = interpolate(lcaProjectedPoint, m_itemForIndex[fromLca.last()]->pos(), i*(1/(size-1)) );
        QPointF projectedP3 = interpolate(m_itemForIndex[p1]->pos(), projectedP1, m_bundlingStrengthFactor);
        path->cubicTo(projectedP2, projectedP2, projectedP3);
        ++i;
    }
}

QPointF RadialGraphicsTreeView::interpolate (QPointF a, QPointF b, qreal value) const
{
    if (value < 0 || value > 1)
        return QPointF();
    return (1-value)*a+value*b;
}
