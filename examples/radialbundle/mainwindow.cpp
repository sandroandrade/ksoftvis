/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtCore/QDebug>

#include <qglobal.h>

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QDirModel>
#else
#include <QtWidgets/QDirModel>
#endif

#include <QtGui/QStandardItemModel>

#define PI 3.14159265358979

Q_DECLARE_METATYPE(QModelIndex)

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->psbResetPan, SIGNAL(clicked()), ui->radialGraphicsTreeView, SLOT(resetPan()));
    connect(ui->psbResetZoom, SIGNAL(clicked()), ui->radialGraphicsTreeView, SLOT(resetZoom()));

    // File system model
    QDirModel *dirModel = new QDirModel(this);
    QDir current = QDir::current();
    current.cd("treemodel");
    ui->treeView->setModel(dirModel);
    ui->treeView->setRootIndex(dirModel->index(current.absolutePath()));
    m_treeModel = dirModel;
    m_treeRootIndex = dirModel->index(current.absolutePath());

    // Dummy tree model
//    QStandardItemModel *dummyModel = new QStandardItemModel(this);
//    QStandardItem *treeParent = new QStandardItem("p");
//    dummyModel->invisibleRootItem()->appendRow(treeParent);
//    buildDummyTreeModel(treeParent, 5, 5);
//    m_treeModel = dummyModel;
//    m_treeRootIndex = treeParent->index();

    // Dummy adjacency model
    m_adjacencyModel = new QStandardItemModel(this);
    QStandardItem *adjacencyRoot = m_adjacencyModel->invisibleRootItem();
    buildDummyAdjacencyModel(adjacencyRoot, m_treeRootIndex);

    // Build scene
    ui->radialGraphicsTreeView->setTreeModel(m_treeModel, m_treeRootIndex);
    ui->radialGraphicsTreeView->setAdjacencyModel(m_adjacencyModel);
    ui->radialGraphicsTreeView->updateTreeScene();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_spbNodeRadius_valueChanged(double value)
{
    ui->radialGraphicsTreeView->setNodeRadius(value);
    ui->radialGraphicsTreeView->updateTreeScene();
}

void MainWindow::on_sldBundlingStrengthFactor_valueChanged(int value)
{
    ui->radialGraphicsTreeView->setBundlingStrengthFactor(value/100.0);
    ui->radialGraphicsTreeView->updateAdjacencyScene();
}

void MainWindow::on_sldAdjacencyEdgePenWidth_valueChanged(int value)
{
    ui->radialGraphicsTreeView->setAdjacencyEdgePenWidth(value/50.0);
    ui->radialGraphicsTreeView->updateAdjacencyScene();
}

void MainWindow::on_chbShowMirroredRadial_toggled(bool value)
{
    ui->radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowMirroredRadialView, value);
    ui->radialGraphicsTreeView->updateTreeScene();
    if (!value)
    {
        m_lastShowAdjacencyModel = ui->chbShowAdjacencyEdges->isChecked();
        ui->radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowAdjacencyModel, false);
        ui->chbShowAdjacencyEdges->setChecked(false);
    }
    else
    {
        ui->radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowAdjacencyModel, m_lastShowAdjacencyModel);
        ui->chbShowAdjacencyEdges->setChecked(m_lastShowAdjacencyModel);
    }
}

void MainWindow::on_chbShowAdjacencyEdges_toggled(bool value)
{
    ui->radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowAdjacencyModel, value);
    ui->radialGraphicsTreeView->updateTreeScene();
}

void MainWindow::on_sldInterSpanSpacing_valueChanged(int value)
{
    ui->radialGraphicsTreeView->setInterSpanSpacing((value*PI)/180.0);
    ui->radialGraphicsTreeView->updateTreeScene();
}

void MainWindow::on_sldInterLevelSpacing_valueChanged(int value)
{
    ui->radialGraphicsTreeView->setInterLevelSpacing(value);
    ui->radialGraphicsTreeView->updateTreeScene();
}

void MainWindow::on_chbShowNodes_toggled(bool value)
{
    ui->radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowNormalRadialViewNode, value);
    ui->radialGraphicsTreeView->updateTreeScene();
}

void MainWindow::on_chbShowEdges_toggled(bool value)
{
    ui->radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowNormalRadialViewEdge, value);
    ui->radialGraphicsTreeView->updateTreeScene();
}

void MainWindow::on_chbShowLevelCircles_toggled(bool value)
{
    ui->radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowNormalRadialViewLevelCircles, value);
    ui->radialGraphicsTreeView->updateTreeScene();
}

void MainWindow::on_sldMirroredRadiusIncrementFactor_valueChanged(int value)
{
    ui->radialGraphicsTreeView->setMirroredRadiusIncrementFactor(value/100.0);
    ui->radialGraphicsTreeView->updateTreeScene();
}

void MainWindow::buildDummyTreeModel(QStandardItem *parent, int childrenPerLevel, int maxLevels, int level)
{
    if (level == maxLevels - 1)
        return;

    for (int i = 0; i < childrenPerLevel; ++i)
    {
        QStandardItem *child = new QStandardItem(QString("Level %1 Child %2").arg(level).arg(i));
        parent->appendRow(child);
        buildDummyTreeModel(child, childrenPerLevel, maxLevels, level+1);
    }
}

void MainWindow::buildDummyAdjacencyModel(QStandardItem *adjacencyParent, const QModelIndex &treeParent)
{
    m_treeLeafIndexes.clear();
    populateTreeLeafIndexes(treeParent);
    int i = 0, size = m_treeLeafIndexes.size();
    while (i < 10)
    {
        QModelIndex index, otherIndex;
        while ((otherIndex = m_treeLeafIndexes.at(qrand() % size)) == (index = m_treeLeafIndexes.at(qrand() % size)));
        QStandardItem *source = new QStandardItem;
        source->setData(QVariant::fromValue(index), Qt::UserRole);
        QStandardItem *target = new QStandardItem;
        target->setData(QVariant::fromValue(otherIndex), Qt::UserRole);
        adjacencyParent->appendRow(QList<QStandardItem *>() << source << target);
        ++i;
    }
}

void MainWindow::populateTreeLeafIndexes(const QModelIndex &index)
{
    int rowCount = m_treeModel->rowCount(index);
    if (rowCount == 0)
        m_treeLeafIndexes.append(index);
    else
        for (int row = 0; row < rowCount; ++row)
            populateTreeLeafIndexes(m_treeModel->index(row, 0, index));
}

