/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef GRAPHICSELLIPSEPIEITEM_H
#define GRAPHICSELLIPSEPIEITEM_H

#include <QtGui/QPainterPath>

#include <qglobal.h>

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QGraphicsEllipseItem>
#else
#include <QtWidgets/QGraphicsEllipseItem>
#endif

#include <ksoftvis_export.h>

class KSOFTVIS_EXPORT GraphicsEllipseRingItem : public QGraphicsEllipseItem
{
public:
    explicit GraphicsEllipseRingItem(qreal x, qreal y, qreal width, qreal height, int startAngle = 0, int spanAngle = 16*360, qreal ringFactor = 1.0, QGraphicsItem *parent = 0);
    virtual ~GraphicsEllipseRingItem();

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    virtual QPainterPath shape() const;
    virtual QRectF boundingRect() const;

    qreal ringFactor() const;

public Q_SLOTS:
    void setRingFactor(qreal ringFactor);

protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);

private:
    QPainterPath buildPainterPath() const;

    qreal m_ringFactor;
};

#endif // GRAPHICSELLIPSEPIEITEM_H
